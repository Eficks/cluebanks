from dataclasses import dataclass

@dataclass
class PostAccountDto:
    user_id:int
    def __init__(self, **entries):
        self.__dict__.update(entries)

@dataclass
class PutAccountDto:
    balance:int
    def __init__(self, **entries):
        self.__dict__.update(entries)

@dataclass
class CreditAccountDto:
    amount:int
    def __init__(self, **entries):
        self.__dict__.update(entries)

@dataclass
class DebitAccountDto:
    amount:int
    def __init__(self, **entries):
        self.__dict__.update(entries)

@dataclass
class TransfertAccountDto:
    amount:int
    def __init__(self, **entries):
        self.__dict__.update(entries)
