from app.mod_users.user_model import UserModel
from flask import jsonify, request
from app import db
import app.mod_users.user_dtos as UserDtos
from typeguard import typechecked
from werkzeug.exceptions import BadRequest
import geocoder
from app.mod_accounts.account_model import AccountModel
from typeguard import typechecked
from typing import Union

@typechecked
def geocode(address:str):
    g = geocoder.google(address)
    if (g):
        return {"lat":g.json.get('lat'), "lng":g.json.get('lng')}
    else:
        raise BadRequest('Invalid address')


@typechecked
def get_users(offset: Union[int,None], limit: Union[int,None]):
    return UserModel.query.order_by(UserModel.id).offset(offset).limit(limit).all()

@typechecked
def get_user(id:int):
    return db.session.query(UserModel).filter(UserModel.id==id).one()

@typechecked
def post_user(user: UserDtos.PostUserDto):
    user = UserModel(**user.__dict__)  
    user.__dict__.update(**geocode(user.address))
    db.session.add(user)
    db.session.commit()
    return user

@typechecked
def put_user(id: int, user: UserDtos.PutUserDto):
    if user.address:
        user.__dict__.update(**geocode(user.address))
    updatedRows=db.session.query(UserModel).filter(UserModel.id==id).update(user.__dict__)
    db.session.commit()
    return {"updated_rows":updatedRows}

@typechecked
def delete_user(id: int):
    user=db.session.query(UserModel).filter(UserModel.id==id).with_for_update().one()
    for account in user.accounts:
        if account.balance>0:
            raise BadRequest('User accounts are not empty')
    deletedRows=db.session.query(UserModel).filter(UserModel.id==id).delete()
    db.session.commit()
    return {"deleted_rows":deletedRows}



