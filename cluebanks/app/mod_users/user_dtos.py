from dataclasses import dataclass
import datetime

@dataclass
class PostUserDto:
    firstname:str
    lastname:str
    address:str
    def __init__(self, **entries):
        self.__dict__.update(entries)

@dataclass
class PutUserDto:
    firstname:str=None
    lastname:str=None
    address:str=None
    birthdate:datetime.date=None
    def __init__(self, **entries):
        self.__dict__.update(entries)
