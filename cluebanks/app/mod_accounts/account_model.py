from app import db
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey
from sqlalchemy_serializer import SerializerMixin


class AccountModel(db.Model, SerializerMixin):
    __tablename__ = 'account'
    __table_args__ = {'mysql_engine':'InnoDB'}

    serialize_only = ('id', 'balance', 'user', 'number')
    serialize_rules = ('-user.accounts',)

    id          : int = db.Column(db.Integer, primary_key=True)
    balance     : int = db.Column(db.Integer, nullable=False)
    number      : str = db.Column(db.String(36), nullable=False, index=True, unique=True)
    user_id     : int = db.Column(db.Integer, ForeignKey('user.id', ondelete="cascade"))

    user        = relationship("UserModel", back_populates="accounts")
