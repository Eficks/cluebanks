import app.mod_accounts.account_service as AccountService
from flask import jsonify, request
import desert
from app.mod_accounts import mod_accounts
import app.mod_accounts.account_dtos as AccountDtos

@mod_accounts.route('/', methods=['GET'])
def get_accounts():
    """Get list of accounts.
    ---
    parameters:
      - name: offset
        in: query
        required: false
        type: number
      - name: limit
        in: query
        required: false
        type: number
    tags:
      - Accounts
    responses:
      200:
        description: A list of accounts 
    """
    return jsonify(AccountService.get_accounts(offset=request.args.get('offset'), limit=request.args.get('limit')))

@mod_accounts.route('/<number>', methods=['GET'])
def get_account(number:str):
    """Get account details.
    ---
    tags:
      - Accounts
    parameters:
      - name: id
        in: path
        required: true
        type: number
    responses:
      200:
        description: A detailed account
    """
    return jsonify(AccountService.get_account(number))

@mod_accounts.route('/', methods=['POST'])
def post_account():
    """
    Create account
    ---
    tags:
      - Accounts
    parameters:
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                user_id:
                    type: number
                    description: The id of the account's owner.

    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: The account inserted in the database
    """    
    return jsonify(AccountService.post_account(desert.schema(AccountDtos.PostAccountDto).load(request.json)))    

@mod_accounts.route('/<number>/credit', methods=['POST'])
def credit_account(number:str):
    """
    Credit an account
    ---
    tags:
      - Accounts
    parameters:
      - name: number
        in: path
        required: true
        type: string
        description: The account's number.
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                amount:
                    type: number
                    description: Amount to be credited.
    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: The updated account
    """    
    return jsonify(AccountService.credit_account(number, desert.schema(AccountDtos.CreditAccountDto).load(request.json)))    

@mod_accounts.route('/<number>/debit', methods=['POST'])
def debit_account(number:str):
    """
    Debit an account
    ---
    tags:
      - Accounts
    parameters:
      - name: number
        in: path
        required: true
        type: string
        description: The account's number.
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                amount:
                    type: number
                    description: Amount to be debited.
    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: The updated account
    """    
    return jsonify(AccountService.debit_account(number, desert.schema(AccountDtos.DebitAccountDto).load(request.json)))    

@mod_accounts.route('/<number_from>/transfer-to/<number_to>', methods=['POST'])
def tranfer_to(number_from:str, number_to:str):
    """
    Transfert money to a different account
    ---
    tags:
      - Accounts
    parameters:
      - name: number_from
        in: path
        required: true
        type: string
        description: The origin account number.
      - name: number_to
        in: path
        required: true
        type: string
        description: The destination account number.
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                amount:
                    type: number
                    description: Amount to be debited.
    responses:
      200:
        schema:
          type: object
          properties:
            debitedAccount:
              type: object
              properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
            creditedAccount:
              type: object
              properties:
                id:
                    type: number
                    description: The account's id.
                number:
                    type: number
                    description: The account's number.
                balance:
                    type: balance
                    description: The account's balance.
        description: Debited and credited accounts
    """    
    return jsonify(AccountService.transfer_account(number_from=number_from, number_to=number_to, transfer=desert.schema(AccountDtos.TransfertAccountDto).load(request.json)))    

@mod_accounts.route('/<number>', methods=['DELETE'])
def delete_account(number:str):
    """
    Delete account
    ---
    tags:
      - Accounts
    parameters:
      - name: number
        in: path
        required: true
        type: string

    responses:
      200:
        schema:
            type: object
            properties:
                deletedRows:
                    type: number
                    description: The number of deleted rows.
        description: The number of deleted rows.
    """
    return jsonify(AccountService.delete_account(number))        



