from app import db
import app.mod_accounts.account_dtos as AccountDtos
from werkzeug.exceptions import BadRequest
from app.mod_users.user_model import UserModel
from app.mod_accounts.account_model import AccountModel
from typeguard import typechecked
from typing import Union
import geocoder
import uuid

@typechecked
def get_accounts(offset:Union[int,None], limit:Union[int,None]):
    return AccountModel.query.outerjoin(UserModel).offset(offset).limit(limit).all()

@typechecked
def get_account(number:str):
    return db.session.query(AccountModel).outerjoin(UserModel).filter(AccountModel.number==number).one()

@typechecked
def post_account(account: AccountDtos.PostAccountDto):
    account = AccountModel(**account.__dict__)  
    account.balance=0
    account.number=str(uuid.uuid4())
    db.session.add(account)
    db.session.commit()
    return account

@typechecked
def credit_account(number: str, credit: AccountDtos.CreditAccountDto):
    if (credit.amount<=0):
        raise BadRequest('Amount must be stricly positive')
    account = db.session.query(AccountModel).filter(AccountModel.number==number).with_for_update().one()
    account.balance+=credit.amount
    db.session.add(account)
    db.session.commit()
    return account

@typechecked
def debit_account(number: str, debit: AccountDtos.DebitAccountDto):
    if (debit.amount<=0):
        raise BadRequest('Amount must be stricly positive')
    account = db.session.query(AccountModel).filter(AccountModel.number==number).with_for_update().one()
    if (account.balance<debit.amount):
        raise BadRequest('Not enough credit')
    account.balance-=debit.amount
    db.session.add(account)
    db.session.commit()
    return account

@typechecked
def transfer_account(number_from: str, number_to: str, transfer: AccountDtos.TransfertAccountDto):
    if (transfer.amount<=0):
        raise BadRequest('Amount must be stricly positive')
    if (number_from==number_to):
        raise BadRequest('Origin account must be different than destination account')
    account_from = db.session.query(AccountModel).filter(AccountModel.number==number_from).with_for_update().one()
    account_to = db.session.query(AccountModel).filter(AccountModel.number==number_to).with_for_update().one()
    if (account_from.balance<transfer.amount):
        raise BadRequest('Not enough credit')
    account_from.balance-=transfer.amount
    account_to.balance+=transfer.amount
    db.session.add(account_from)
    db.session.add(account_to)
    db.session.commit()
    return {"debited_account": account_from, "credited_account": account_to}

@typechecked
def delete_account(number: str):
    account = db.session.query(AccountModel).filter(AccountModel.number==number).with_for_update().one()
    if account.balance>0:
        raise BadRequest('Account is not empty')
    deletedRows=db.session.query(AccountModel).filter(AccountModel.number==number).delete()
    db.session.commit()
    return {"deleted_rows":deletedRows}