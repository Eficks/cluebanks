from app import db
from flask import jsonify, request
from app.mod_users import mod_users
import app.mod_users.user_service as UserService
import app.mod_users.user_dtos as UserDtos
import desert

@mod_users.route('/', methods=['GET'])
def get_users():
    """Get list of users.
    ---
    parameters:
      - name: offset
        in: query
        required: false
        type: number
      - name: limit
        in: query
        required: false
        type: number
    tags:
      - Users
    responses:
      200:
        description: A list of users 
    """
    return jsonify(UserService.get_users(offset=request.args.get('offset'), limit=request.args.get('limit')))

@mod_users.route('/<int:id>', methods=['GET'])
def get_user(id:int):
    """Get user details.
    ---
    tags:
      - Users
    parameters:
      - name: id
        in: path
        required: true
        type: number
    responses:
      200:
        description: A detailed user
    """
    return jsonify(UserService.get_user(id))
     
    
@mod_users.route('/', methods=['POST'])
def post_user():
    """
    Create user
    ---
    tags:
      - Users
    parameters:
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                firstname:
                    type: string
                    description: The user's firstname.
                lastname:
                    type: string
                    description: The user's lastname.
                address:
                    type: string
                    description: The user's address.

    responses:
      200:
        schema:
            type: object
            properties:
                id:
                    type: number
                    description: The user's id.
                firstname:
                    type: string
                    description: The user's firstname.
                lastname:
                    type: string
                    description: The user's lastname.
                address:
                    type: string
                    description: The user's address.
        description: The user inserted in the database
    """
    return jsonify(UserService.post_user(desert.schema(UserDtos.PostUserDto).load(request.json)))    

@mod_users.route('/<int:id>', methods=['PUT'])
def put_user(id:int):
    """
    Update user
    ---
    tags:
      - Users
    parameters:
      - name: body
        in: body
        required: true
        schema:
            type: object
            properties:
                firstname:
                    type: string
                    description: The user's firstname.
                lastname:
                    type: string
                    description: The user's lastname.
                address:
                    type: string
                    description: The user's address.
      - name: id
        in: path
        required: true
        type: number

    responses:
      200:
        schema:
            type: object
            properties:
                updatedRows:
                    type: number
                    description: The number of updated rows.
        description: The number of updated rows.
    """
    return jsonify(UserService.put_user(id, desert.schema(UserDtos.PutUserDto).load(request.json)))    

@mod_users.route('/<int:id>', methods=['DELETE'])
def delete_user(id:int):
    """
    Delete user
    ---
    tags:
      - Users
    parameters:
      - name: id
        in: path
        required: true
        type: number

    responses:
      200:
        schema:
            type: object
            properties:
                deletedRows:
                    type: number
                    description: The number of deleted rows.
        description: The number of deleted rows.
    """
    return jsonify(UserService.delete_user(id))        






    # print(swagger.get_schema('user'))
    # print(desert.schema(UserDtos.User))

    # return jsonify(UserService.post_user(swagger.get_schema('user').load(request.json)))    
