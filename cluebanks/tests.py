import unittest
import flask_testing
from flask import Flask
from flask_testing import TestCase
from app import app

class TestViews(TestCase):
    def create_app(self):
        # app = Flask(__name__)
        # app.config['TESTING'] = True
        # app.config['REMOTE_PORT'] = 80
        # app.config['URL'] = 'http://localhost'
        return app

    def test_it(self):
        user1 = self.client.post('/users/', json={
            "firstname":"François-Xavier",
            "lastname":"De Nys",
            "address":"Kamerdelle 30"
        }, follow_redirects=True).get_json()

        user2 = self.client.post('/users/', json={
            "firstname":"Jean",
            "lastname":"Dupont",
            "address":"Avenue de fré 50"
        }, follow_redirects=True).get_json()

        user1 = self.client.get('/users/'+str(user1['id']), follow_redirects=True).get_json()
        user2 = self.client.get('/users/'+str(user2['id']), follow_redirects=True).get_json()

        self.assertEqual(user1['firstname'], 'François-Xavier')
        self.assertEqual(user1['lastname'], 'De Nys')
        self.assertEqual(user1['address'], 'Kamerdelle 30')
        self.assertAlmostEqual(user1['lat'], 50.8017, 4)
        self.assertAlmostEqual(user1['lng'], 4.34804, 4)

        self.assertEqual(user2['firstname'], 'Jean')
        self.assertEqual(user2['lastname'], 'Dupont')
        self.assertEqual(user2['address'], 'Avenue de fré 50')
        self.assertAlmostEqual(user2['lat'], 50.8038, 4)
        self.assertAlmostEqual(user2['lng'], 4.34825, 4)

        response=self.client.put('/users/'+str(user2['id']), json={
            "firstname":"Albert",
            "lastname":"Durant",
            "birthdate":"1974-12-26",
            "address":"Bastogne"
        }, follow_redirects=True).get_json()
        self.assertEqual(response['updated_rows'], 1)

        user2 = self.client.get('/users/'+str(user2['id']), follow_redirects=True).get_json()
        self.assertEqual(user2['firstname'], 'Albert')
        self.assertEqual(user2['lastname'], 'Durant')
        self.assertEqual(user2['address'], 'Bastogne')
        self.assertEqual(user2['birthdate'], '1974-12-26')
        self.assertAlmostEqual(user2['lat'], 50.0005, 4)
        self.assertAlmostEqual(user2['lng'], 5.71528, 4)

        user3 = self.client.post('/users/', json={
            "firstname":"Bidon",
            "lastname":"Bidon",
            "address":"Namur"
        }, follow_redirects=True).get_json()

        response = self.client.delete('/users/'+str(user3['id']), follow_redirects=True).get_json()
        self.assertEqual(response['deleted_rows'], 1)

        user1_account1 = self.client.post('/accounts/', json={
            "user_id": user1['id']
        }, follow_redirects=True).get_json()
        user1_account2 = self.client.post('/accounts/', json={
            "user_id": user1['id']
        }, follow_redirects=True).get_json()
        user2_account1 = self.client.post('/accounts/', json={
            "user_id": user2['id']
        }, follow_redirects=True).get_json()


        user1_account1 = self.client.get('/accounts/'+str(user1_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(user1_account1['user']['lastname'], user1['lastname'])
        self.assertEqual(user1_account1['balance'], 0)

        user1_account2 = self.client.get('/accounts/'+str(user1_account2['number']), follow_redirects=True).get_json()
        self.assertEqual(user1_account2['user']['lastname'], user1['lastname'])
        self.assertEqual(user1_account2['balance'], 0)

        user2_account1 = self.client.get('/accounts/'+str(user2_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(user2_account1['user']['lastname'], user2['lastname'])
        self.assertEqual(user2_account1['balance'], 0)

        response = self.client.post('/accounts/'+user1_account1['number']+'/credit', json={
            "amount": 100
        }, follow_redirects=True).get_json()
        self.assertEqual(response['balance'], 100)

        user1_account1 = self.client.get('/accounts/'+str(user1_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(user1_account1['balance'], 100)

        response = self.client.post('/accounts/'+user1_account1['number']+'/credit', json={
            "amount": 50
        }, follow_redirects=True).get_json()
        self.assertEqual(response['balance'], 150)
        
        user1_account1 = self.client.get('/accounts/'+str(user1_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(user1_account1['balance'], 150)

        response = self.client.post('/accounts/'+user1_account1['number']+'/debit', json={
            "amount": 140
        }, follow_redirects=True).get_json()
        self.assertEqual(response['balance'], 10)
        
        user1_account1 = self.client.get('/accounts/'+str(user1_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(user1_account1['balance'], 10)

        response = self.client.post('/accounts/'+user1_account1['number']+'/debit', json={
            "amount": 20
        }, follow_redirects=True).get_json()
        self.assertEqual(response['code'], 400)
        
        response = self.client.post('/accounts/'+user1_account1['number']+'/transfer-to/'+user2_account1['number'], json={
            "amount": 10
        }, follow_redirects=True).get_json()
        self.assertEqual(response['debited_account']['balance'], 0)
        self.assertEqual(response['credited_account']['balance'], 10)

        user1_account1 = self.client.get('/accounts/'+str(user1_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(user1_account1['balance'], 0)

        user2_account1 = self.client.get('/accounts/'+str(user2_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(user2_account1['balance'], 10)

        response = self.client.post('/accounts/'+user1_account1['number']+'/transfer-to/'+user2_account1['number'], json={
            "amount": 10
        }, follow_redirects=True).get_json()
        self.assertEqual(response['code'], 400)

        response = self.client.delete('/accounts/'+str(user1_account2['number']), follow_redirects=True).get_json()
        self.assertEqual(response['deleted_rows'], 1)

        response = self.client.delete('/users/'+str(user1['id']), follow_redirects=True).get_json()
        self.assertEqual(response['deleted_rows'], 1)

        response = self.client.delete('/users/'+str(user2['id']), follow_redirects=True).get_json()
        self.assertEqual(response['code'], 400)

        response = self.client.delete('/accounts/'+str(user2_account1['number']), follow_redirects=True).get_json()
        self.assertEqual(response['code'], 400)

        response = self.client.post('/accounts/'+user2_account1['number']+'/debit', json={
            "amount": 10
        }, follow_redirects=True).get_json()
        self.assertEqual(response['balance'], 0)

        response = self.client.delete('/users/'+str(user2['id']), follow_redirects=True).get_json()
        self.assertEqual(response['deleted_rows'], 1)


if __name__ == '__main__':
    unittest.main()
    app.run(host='0.0.0.0', port=8080, debug=False)