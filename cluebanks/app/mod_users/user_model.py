from app import db
from sqlalchemy.orm import relationship
from sqlalchemy_serializer import SerializerMixin
import datetime
import json


class UserModel(db.Model, SerializerMixin):
    __tablename__ = 'user'
    __table_args__ = {'mysql_engine':'InnoDB'}

    serialize_only = ('id', 'firstname', 'lastname', 'address', 'lat', 'lng', 'birthdate', 'accounts')
    serialize_rules = ('-accounts.user',)

    id          : int = db.Column(db.Integer, primary_key=True)
    firstname   : str = db.Column(db.String(128), nullable=False)
    lastname    : str = db.Column(db.String(128), nullable=False)
    address     : str = db.Column(db.String(256), nullable=True)
    lat         : float = db.Column(db.Float(), nullable=True)
    lng         : float = db.Column(db.Float(), nullable=True)
    birthdate   : datetime.date = db.Column(db.Date(), nullable=True)

    accounts    = relationship("AccountModel", back_populates="user", lazy='joined')
