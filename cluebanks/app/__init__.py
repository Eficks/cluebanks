from flask import Flask, render_template, json, make_response
from flask_sqlalchemy  import SQLAlchemy
from flasgger import Swagger
from werkzeug.exceptions import HTTPException
from marshmallow.exceptions import ValidationError
from sqlalchemy.ext.declarative import DeclarativeMeta
from flask.json import JSONEncoder
from app.my_encoder import MyEncoder

app = Flask(__name__)

swagger = Swagger(app)

app.config.from_object('config')
app.json_encoder = MyEncoder

db = SQLAlchemy(app)

@app.errorhandler(Exception)
def handle_exception(e):
    response = make_response(json.dumps({
        "code": 400,
        "name": type(e).__name__,
        "description": str(e) 
    }), 400)
    response.content_type = "application/json"
    return response

@app.errorhandler(HTTPException)
def handle_exception(e):
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response

from app.mod_users.user_controller import mod_users as users_module
from app.mod_accounts.account_controller import mod_accounts as accounts_module
app.register_blueprint(users_module)
app.register_blueprint(accounts_module)

db.create_all()

